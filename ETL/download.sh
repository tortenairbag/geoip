#!/bin/bash
#
# download.sh
#
# Description:
# Downloads the zip-file from the GeoLite2 Server
#
# Parameters:
# 1. URL
# 2. Output directory
# 2. Output file

GEO_IP_IMPORT_URL=$1
GEO_IP_IMPORT_DIR=$2
GEO_IP_IMPORT_FILE=$3

# Check Input Parameters
if [ -z "$GEO_IP_IMPORT_URL" ] || [ -z "$GEO_IP_IMPORT_DIR" ] || [ -z "$GEO_IP_IMPORT_FILE" ]; then 
	echo "Missing parameters!"
	exit 1
fi

# Download file
wget $GEO_IP_IMPORT_URL -O $GEO_IP_IMPORT_DIR/$GEO_IP_IMPORT_FILE
exit 0
