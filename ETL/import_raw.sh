#!/bin/bash
#
# import_raw.sh
#
# Description:
# Uploads the input files to HDFS
#
# Parameters:
# 1. Import file directory
# 2. Import file location prefix
# 3. Language ISO codes, seperated with a semicolon
# 4. Import file blocks prefix
# 5. IP Blocks, seperated with a semicolon
# 6. HDFS directory
# 7. Year
# 8. Month
# 9. Day

GEO_IP_IMPORT_DIR=$1
GEO_IP_IMPORT_LOCATIONS_FILE_PREFIX=$2
GEO_IP_IMPORT_LOCATIONS=$3
GEO_IP_IMPORT_BLOCKS_FILE_PREFIX=$4
GEO_IP_IMPORT_BLOCKS=$5
GEO_IP_IMPORT_HDFS_DIR=$6
YEAR=$7
MONTH=$8
DAY=$9

# Check Input Parameters
if [ -z "$GEO_IP_IMPORT_DIR" ] \
	|| [ -z "$GEO_IP_IMPORT_LOCATIONS_FILE_PREFIX" ] \
	|| [ -z "$GEO_IP_IMPORT_LOCATIONS" ] \
	|| [ -z "$GEO_IP_IMPORT_BLOCKS_FILE_PREFIX" ] \
	|| [ -z "$GEO_IP_IMPORT_BLOCKS" ] \
	|| [ -z "$GEO_IP_IMPORT_HDFS_DIR" ] \
	|| [ -z "$YEAR" ] \
	|| [ -z "$MONTH" ] \
	|| [ -z "$DAY" ]; then 
	echo "Missing parameters!"
	exit 1
fi

# Upload location files
GEO_IP_IMPORT_LOCATIONS=$(echo $GEO_IP_IMPORT_LOCATIONS | tr \; \\n)
for location in ${GEO_IP_IMPORT_LOCATIONS[@]}; do
	# Create folder in hdfs
	hdfs_dir="$GEO_IP_IMPORT_HDFS_DIR/raw/locations/$YEAR/$MONTH/$DAY/$location/"
	/home/hadoop/hadoop/bin/hadoop fs -mkdir -p $hdfs_dir

	# Upload file in hdfs
	filename="$GEO_IP_IMPORT_LOCATIONS_FILE_PREFIX-$location.csv"
	/home/hadoop/hadoop/bin/hadoop fs -put -f "$GEO_IP_IMPORT_DIR/$filename" "$hdfs_dir/$filename"
done

# Upload block files
GEO_IP_IMPORT_BLOCKS=$(echo $GEO_IP_IMPORT_BLOCKS | tr \; \\n)
for block in ${GEO_IP_IMPORT_BLOCKS[@]}; do
	# Create folder in hdfs
	hdfs_dir="$GEO_IP_IMPORT_HDFS_DIR/raw/blocks/$YEAR/$MONTH/$DAY/$block/"
	/home/hadoop/hadoop/bin/hadoop fs -mkdir -p $hdfs_dir

	# Upload file in hdfs
	filename="$GEO_IP_IMPORT_BLOCKS_FILE_PREFIX-$block.csv"
	/home/hadoop/hadoop/bin/hadoop fs -put -f "$GEO_IP_IMPORT_DIR/$filename" "$hdfs_dir/$filename"
done

