#!/bin/bash
#
# check_files.sh
#
# Description:
# Checks, if the input files exists and if they are not empty
#
# Parameters:
# 1. Import file directory
# 2. Import file location prefix
# 3. Language ISO codes, seperated with a semicolon
# 4. Import file blocks prefix
# 5. IP Blocks, seperated with a semicolon
# 6. HDFS directory

GEO_IP_IMPORT_DIR=$1
GEO_IP_IMPORT_LOCATIONS_FILE_PREFIX=$2
GEO_IP_IMPORT_LOCATIONS=$3
GEO_IP_IMPORT_BLOCKS_FILE_PREFIX=$4
GEO_IP_IMPORT_BLOCKS=$5

# Check Input Parameters
if [ -z "$GEO_IP_IMPORT_DIR" ] \
	|| [ -z "$GEO_IP_IMPORT_LOCATIONS_FILE_PREFIX" ] \
	|| [ -z "$GEO_IP_IMPORT_LOCATIONS" ] \
	|| [ -z "$GEO_IP_IMPORT_BLOCKS_FILE_PREFIX" ] \
	|| [ -z "$GEO_IP_IMPORT_BLOCKS" ]; then 
	echo "Missing parameters!"
	exit 1
fi

# Check location files
GEO_IP_IMPORT_LOCATIONS=$(echo $GEO_IP_IMPORT_LOCATIONS | tr \; \\n)
for location in ${GEO_IP_IMPORT_LOCATIONS[@]}; do
	filename="$GEO_IP_IMPORT_LOCATIONS_FILE_PREFIX-$location.csv"
	if [ ! -f "$GEO_IP_IMPORT_DIR/$filename" ]; then
		echo "File not found: $filename"
		exit 1
	fi
	if [ ! -s "$GEO_IP_IMPORT_DIR/$filename" ]; then
		echo "File is empty: $filename"
		exit 1
	fi
done

# Check block files
GEO_IP_IMPORT_BLOCKS=$(echo $GEO_IP_IMPORT_BLOCKS | tr \; \\n)
for block in ${GEO_IP_IMPORT_BLOCKS[@]}; do
	filename="$GEO_IP_IMPORT_BLOCKS_FILE_PREFIX-$block.csv"
	if [ ! -f "$GEO_IP_IMPORT_DIR/$filename" ]; then
		echo "File not found: $filename"
		exit 1
	fi
	if [ ! -s "$GEO_IP_IMPORT_DIR/$filename" ]; then
		echo "File is empty: $filename"
		exit 1
	fi
done

exit 0
