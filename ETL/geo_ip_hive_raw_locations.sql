CREATE TABLE ${GEO_IP_TABLE_HIVE_RAW_LOCATIONS} (
	geoname_id INT,
	locale_code STRING,
	continent_code STRING,
	continent_name STRING,
	country_iso_code STRING,
	country_name STRING,
	subdivision_1_iso_code STRING,
	subdivision_1_name STRING,
	subdivision_2_iso_code STRING,
	subdivision_2_name STRING,
	city_name STRING,
	metro_code STRING,
	time_zone STRING,
	is_in_european_union BOOLEAN)
PARTITIONED BY ( 
	partition_year INT,
	partition_month INT,
	partition_day INT,
	partition_location STRING)
ROW FORMAT SERDE 
	'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
	'field.delim'=',', 
	'serialization.format'=',') 
STORED AS INPUTFORMAT 
	'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
	'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
	'hdfs://${GEO_IP_CONNECTION_HDFS_SERVER}:${GEO_IP_CONNECTION_HDFS_PORT}/${GEO_IP_IMPORT_HDFS_DIR}/raw/locations/'
TBLPROPERTIES (
	'skip.header.line.count'='1'
)

