CREATE TABLE ${GEO_IP_TABLE_HIVE_RAW_BLOCKS} (
	network STRING,
	geoname_id INT,
	registered_country_geoname_id INT,
	represented_country_geoname_id INT,
	is_anonymous_proxy BOOLEAN,
	is_satellite_provider BOOLEAN,
	postal_code STRING,
	latitude DECIMAL(4,4),
	longitude DECIMAL(4,4),
	accuracy_radius INT)
PARTITIONED BY ( 
	partition_year INT,
	partition_month INT,
	partition_day INT,
	partition_block STRING)
ROW FORMAT SERDE 
	'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
	'field.delim'=',', 
	'serialization.format'=',') 
STORED AS INPUTFORMAT 
	'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
	'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
	'hdfs://${GEO_IP_CONNECTION_HDFS_SERVER}:${GEO_IP_CONNECTION_HDFS_PORT}/${GEO_IP_IMPORT_HDFS_DIR}/raw/blocks/'
TBLPROPERTIES (
	'skip.header.line.count'='1'
)

