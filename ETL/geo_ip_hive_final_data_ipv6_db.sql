CREATE TABLE ${GEO_IP_TABLE_HIVE_FINAL_DATA_IPV6} (
	network STRING,
	subnet_begin_1 DECIMAL(24,0),
	subnet_begin_2 DECIMAL(24,0),
	subnet_end_1 DECIMAL(24,0),
	subnet_end_2 DECIMAL(24,0),
	subnet_mask INT,
	postal_code STRING,
	latitude DECIMAL(4,4),
	longitude DECIMAL(4,4),
	accuracy_radius INT,
	geoname_id INT)
ROW FORMAT SERDE 
	'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
	'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
	'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
	'hdfs://${GEO_IP_CONNECTION_HDFS_SERVER}:${GEO_IP_CONNECTION_HDFS_PORT}/${GEO_IP_IMPORT_HDFS_DIR}/final/data_IPv6'
TBLPROPERTIES (
	'skip.header.line.count'='1'
)
