SET hive.exec.dynamic.partition.mode=nonstrict;
INSERT OVERWRITE TABLE ${GEO_IP_TABLE_HIVE_FINAL_LOCATIONS} partition(partition_location)
SELECT
	geoname_id,
	continent_code,
	continent_name,
	country_iso_code,
	country_name,
	city_name,
	metro_code,
	time_zone,
	partition_location
FROM ${GEO_IP_TABLE_HIVE_RAW_LOCATIONS}
WHERE partition_year = ${YEAR}
	AND partition_month = ${MONTH}
	AND partition_day = ${DAY};

