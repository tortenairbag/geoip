#!/bin/bash
#
# import_partitions.sh
#
# Description:
# Imports HDFS database partitions
#
# Parameters:
#  1. Hive Server
#  2. Hive Port
#  3. Hive Database
#  4. Hive Username
#  5. Hive Password
#  6. HDFS directory
#  7. Language ISO codes, seperated with a semicolon
#  8. IP Blocks, seperated with a semicolon
#  9. Locations table name
# 10. Blocks table name
# 11. Year
# 12. Month
# 13. Day
GEO_IP_CONNECTION_HIVE_SERVER=$1
GEO_IP_CONNECTION_HIVE_PORT=$2
GEO_IP_CONNECTION_HIVE_DATABASE=$3
GEO_IP_CONNECTION_HIVE_USERNAME=$4
GEO_IP_CONNECTION_HIVE_PASSWORD=$5
GEO_IP_IMPORT_HDFS_DIR=$6
GEO_IP_IMPORT_LOCATIONS=$7
GEO_IP_IMPORT_BLOCKS=$8
GEO_IP_TABLE_HIVE_RAW_LOCATIONS=$9
GEO_IP_TABLE_HIVE_RAW_BLOCKS=${10}
YEAR=${11}
MONTH=${12}
DAY=${13}
query=""

# Check Input Parameters
if [ -z "$GEO_IP_CONNECTION_HIVE_SERVER" ] \
	|| [ -z "$GEO_IP_CONNECTION_HIVE_PORT" ] \
	|| [ -z "$GEO_IP_CONNECTION_HIVE_DATABASE" ] \
	|| [ -z "$GEO_IP_CONNECTION_HIVE_USERNAME" ] \
	|| [ -z "$GEO_IP_CONNECTION_HIVE_PASSWORD" ] \
	|| [ -z "$GEO_IP_IMPORT_HDFS_DIR" ] \
	|| [ -z "$GEO_IP_IMPORT_LOCATIONS" ] \
	|| [ -z "$GEO_IP_IMPORT_BLOCKS" ] \
	|| [ -z "$GEO_IP_TABLE_HIVE_RAW_LOCATIONS" ] \
	|| [ -z "$GEO_IP_TABLE_HIVE_RAW_BLOCKS" ] \
	|| [ -z "$YEAR" ] \
	|| [ -z "$MONTH" ] \
	|| [ -z "$DAY" ]; then 
	echo "Missing parameters!"
	exit 1
fi

# Check location files
GEO_IP_IMPORT_LOCATIONS=$(echo $GEO_IP_IMPORT_LOCATIONS | tr \; \\n)
for location in ${GEO_IP_IMPORT_LOCATIONS[@]}; do
	hdfs_dir="$GEO_IP_IMPORT_HDFS_DIR/raw/locations/$YEAR/$MONTH/$DAY/$location"
	query="$query
		ALTER TABLE $GEO_IP_TABLE_HIVE_RAW_LOCATIONS 
		ADD IF NOT EXISTS partition(partition_year=$YEAR, partition_month=$MONTH, partition_day=$DAY, partition_location='$location')
		LOCATION '$hdfs_dir';"
done

# Check block files
GEO_IP_IMPORT_BLOCKS=$(echo $GEO_IP_IMPORT_BLOCKS | tr \; \\n)
for block in ${GEO_IP_IMPORT_BLOCKS[@]}; do
	hdfs_dir="$GEO_IP_IMPORT_HDFS_DIR/raw/blocks/$YEAR/$MONTH/$DAY/$block/"
	query="$query
		ALTER TABLE $GEO_IP_TABLE_HIVE_RAW_BLOCKS 
		ADD IF NOT EXISTS partition(partition_year=$YEAR, partition_month=$MONTH, partition_day=$DAY, partition_block='$block')
		LOCATION '$hdfs_dir';"
done

query=$(echo $query | tr \\n " ")
/home/hadoop/hive/bin/beeline -u "jdbc:hive2://$GEO_IP_CONNECTION_HIVE_SERVER:$GEO_IP_CONNECTION_HIVE_PORT/$GEO_IP_CONNECTION_HIVE_DATABASE" -n "$GEO_IP_CONNECTION_HIVE_USERNAME" -p "$GEO_IP_CONNECTION_HIVE_PASSWORD" -e "$query"

