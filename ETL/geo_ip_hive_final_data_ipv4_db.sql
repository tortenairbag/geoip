CREATE TABLE ${GEO_IP_TABLE_HIVE_FINAL_DATA_IPV4} (
	network STRING,
	subnet_begin DECIMAL(12,0),
	subnet_end DECIMAL(12,0),
	subnet_mask INT,
	postal_code STRING,
	latitude DECIMAL(4,4),
	longitude DECIMAL(4,4),
	accuracy_radius INT,
	geoname_id INT)
ROW FORMAT SERDE 
	'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
	'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
	'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
	'hdfs://${GEO_IP_CONNECTION_HDFS_SERVER}:${GEO_IP_CONNECTION_HDFS_PORT}/${GEO_IP_IMPORT_HDFS_DIR}/final/data_IPv4'
TBLPROPERTIES (
	'skip.header.line.count'='1'
)
