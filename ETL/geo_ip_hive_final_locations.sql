CREATE TABLE ${GEO_IP_TABLE_HIVE_FINAL_LOCATIONS} (
	geoname_id INT,
	continent_code STRING,
	continent_name STRING,
	country_iso_code STRING,
	country_name STRING,
	city_name STRING,
	metro_code STRING,
	time_zone STRING)
PARTITIONED BY ( 
	partition_location STRING)
ROW FORMAT SERDE 
	'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
	'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
	'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
	'hdfs://${GEO_IP_CONNECTION_HDFS_SERVER}:${GEO_IP_CONNECTION_HDFS_PORT}/${GEO_IP_IMPORT_HDFS_DIR}/final/locations'
