#!/bin/bash
#
# import_final_blocks.sh
#
# Description:
# Uploads the generated files for the final layer to HDFS
#
# Parameters:
# 1. Import file directory
# 2. Import file name prefix
# 3. IP Blocks, seperated with a semicolon
# 4. HDFS directory

GEO_IP_IMPORT_DIR=$1
GEO_IP_EXPORT_BLOCKS_FILE_PREFIX=$2
GEO_IP_IMPORT_BLOCKS=$3
GEO_IP_IMPORT_HDFS_DIR=$4

# Check Input Parameters
if [ -z "$GEO_IP_IMPORT_DIR" ] \
	|| [ -z "$GEO_IP_EXPORT_BLOCKS_FILE_PREFIX" ] \
	|| [ -z "$GEO_IP_IMPORT_BLOCKS" ] \
	|| [ -z "$GEO_IP_IMPORT_HDFS_DIR" ]; then
	echo "Missing parameters!"
	exit 1
fi

# Upload block files
GEO_IP_IMPORT_BLOCKS=$(echo $GEO_IP_IMPORT_BLOCKS | tr \; \\n)
for block in ${GEO_IP_IMPORT_BLOCKS[@]}; do
	# Create folder in hdfs
	hdfs_dir="$GEO_IP_IMPORT_HDFS_DIR/final/data_$block"
	/home/hadoop/hadoop/bin/hadoop fs -mkdir -p $hdfs_dir

	# Upload file in hdfs
	filename="$GEO_IP_EXPORT_BLOCKS_FILE_PREFIX-$block.csv"
	/home/hadoop/hadoop/bin/hadoop fs -put -f "$GEO_IP_IMPORT_DIR/$filename" "$hdfs_dir/$filename"
done

