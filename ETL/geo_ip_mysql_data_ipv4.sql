CREATE TABLE ${GEO_IP_TABLE_MYSQL_DATA_IPV4} (
	network VARCHAR(18),
	subnet_begin INT(12) UNSIGNED,
	subnet_end INT(12) UNSIGNED,
	subnet_mask INT(11),
	postal_code VARCHAR(64),
	latitude DECIMAL(4,4),
	longitude DECIMAL(4,4),
	accuracy_radius INT,
	geoname_id INT,
	INDEX(subnet_begin, subnet_end) USING BTREE)
