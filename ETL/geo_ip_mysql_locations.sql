CREATE TABLE ${GEO_IP_TABLE_MYSQL_LOCATIONS} (
	geoname_id INT,
	continent_code VARCHAR(8),
	continent_name VARCHAR(64),
	country_iso_code VARCHAR(8),
	country_name VARCHAR(256),
	city_name VARCHAR(256),
	metro_code VARCHAR(64),
	time_zone VARCHAR(256),
	partition_location VARCHAR(8),
	PRIMARY KEY (geoname_id, partition_location))
PARTITION BY KEY (partition_location)
