CREATE TABLE ${GEO_IP_TABLE_MYSQL_DATA_IPV6} (
	network VARCHAR(43),
	subnet_begin_1 BIGINT UNSIGNED,
	subnet_begin_2 BIGINT UNSIGNED,
	subnet_end_1 BIGINT UNSIGNED,
	subnet_end_2 BIGINT UNSIGNED,
	subnet_mask INT,
	postal_code VARCHAR(64),
	latitude DECIMAL(4,4),
	longitude DECIMAL(4,4),
	accuracy_radius INT,
	geoname_id INT,
	INDEX(subnet_begin_1, subnet_begin_2, subnet_end_1, subnet_end_2) USING BTREE)
