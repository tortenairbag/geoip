# GEO IP

## Grober Entwurf
Von *MaxMind* werden `csv`-Dateien zur Verfügung gestellt, welche in Standort- und IP-Adressen-Daten unterteilt werden können.
Dabei werden die Daten für IPv4 und IPv6 sowie die Standorte in verschiedenen Sprachen in separate Dateien gespeichert.
Die Struktur der `zip`-Datei kann wie folgt dargestellt werden:

    GeoLite.zip
    |-- Block-IPv4.csv
    |-- Block-IPv6.csv
    |-- Locations-en.csv
    |-- Locations-de.csv
    |-- ...

Die Struktur der `csv`-Dateien ist dabei innerhalb ihrer Gruppe identisch.
Somit können beim Import in den `raw`-Layer in einer Datenbank-Tabelle mit mehreren Partitionen gespeichert werden.

Die Hauptfunktion des Projekts darin besteht, eine IP-Adresssuche zu implementieren.
Da die IP-Subnetze im Format `62.42.102.128/25` bzw. `2001:268:c03a:8000::/50` angegeben werden, ist eine reine String-Suche unmöglich.
Deshalb müssen die IP-Adressen in Zahlen encoded und über die Subnetz-Maske der Bereich von der ersten bis zur letzten IP-Adresse ermittelt werden.
Mithilfe der ersten und letzten IP-Adresse kann über die `BETWEEN`-Suche ein Subnetz für eine IP-Adresse ermittelt werden.

Diese Transformation bedient sich aus dem `raw`-Layer und speichert die resultierenden Daten in der finalen Struktur in den `final`-Layer.
Anschließend müssen die Daten nur noch von der HIVE-Datenbank in die MySQL-Datenbank exportiert werden.

Über Parameter soll es möglich sein anzugeben, welche Sprachen und welche Adressraum-Versionen aktualisiert werden sollen.
Um einen möglichen Datenverlust zu vermeiden, werden im `final`-Layer nur die Daten gelöscht, welche über den Parameter aktualisiert werden.

## Dateien
| Datei | Beschreibung |
|---|---|
| `check_files.sh` | Überprüft, ob alle angegebenen Sprach- und IP-Dateien heruntergeladen wurden |
| `download.sh` | Lädt die `zip`-Datei herunter |
| `geo_ip_check_table_empty_final.ktr` | Überprüft, ob eine Tabelle leer ist |
| `geo_ip_check_table_empty_raw.ktr` | Überprüft, ob eine Tabelle im `raw`-Layer leer ist. Berücksichtigt die Uploaddatums-Partitionen. |
| `geo_ip_download.kjb` | Downloaded und Entpackt die `zip`-Datei |
| `geo_ip_hive_final_data_ipv4_file.sql` | Erstellt die IPv4-Tabelle im `final`-Layer. Für die Verwendung muss das Kapitel [HIVE-Workaround](#HIVE-Workaround) beachtet werden. |
| `geo_ip_hive_final_data_ipv4_file.sql` | Erstellt die IPv4-Tabelle im `final`-Layer. Für die Verwendung muss das Kapitel [HIVE-Workaround](#HIVE-Workaround) beachtet werden. |
| `geo_ip_hive_final_data_ipv6_file.sql` | Erstellt die IPv6-Tabelle im `final`-Layer. Für die Verwendung muss das Kapitel [HIVE-Workaround](#HIVE-Workaround) beachtet werden. |
| `geo_ip_hive_final_data_ipv6_file.sql` | Erstellt die IPv6-Tabelle im `final`-Layer. Für die Verwendung muss das Kapitel [HIVE-Workaround](#HIVE-Workaround) beachtet werden. |
| `geo_ip_hive_final_locations.sql` | Erstellt die Standort-Tabelle im `final`-Layer |
| `geo_ip_hive_raw_blocks.sql` | Erstellt die IP-Adressen-Tabelle im `raw`-Layer |
| `geo_ip_hive_raw_locations.sql` | Erstellt die Standort-Tabelle im `raw`-Layer |
| `geo_ip_import_final_db.kjb` | Bereinigt die Daten im `raw`-Layer und speichert sie direkt in die Tabellen des `final`-Layer |
| `geo_ip_import_final_db.ktr` | Bereinigt die Daten im `raw`-Layer und schreibt sie in den `final`-Layer |
| `geo_ip_import_final_file.kjb` | Bereinigt die Daten im `raw`-Layer und speichert sie in den `final`-Layer. Es wird eine `csv`-Datei generiert, welche in das HDFS hochgeladen wird. |
| `geo_ip_import_final_file.ktr` | Bereinigt die Daten im `raw`-Layer und erstellt eine `csv`-Datei |
| `geo_ip_import_mysql.kjb` | Exportiert die Daten aus den `final`-Layer in die MySQL Datenbank |
| `geo_ip_import_mysql.ktr` | Exportiert eine Tabelle in die MySQL Datenbank |
| `geo_ip_import_raw.kjb` | Importiert die heruntergeladenen Dateien in die HIVE Datenbank |
| `geo_ip_main.kjb` | Lädt die Daten herunter und speichert sie in die HIVE- und MySQL-Datenbank |
| `geo_ip_mysql_data_ipv4.sql` | Erstellt die IPv4-Tabelle in der MySQL-Datenbank |
| `geo_ip_mysql_data_ipv6.sql` | Erstellt die IPv6-Tabelle in der MySQL-Datenbank |
| `geo_ip_mysql_locations.sql` | Erstellt die Standort-Tabelle in der MySQL-Datenbank |
| `import_final_locations.sql` | Bereinigt die Standort-Daten im `raw`-Layer und speichert sie in den `final`-Layer |
| `import_final_blocks.sh` | Lädt die generierten `csv`-Datei der IP-Adressen in das HDFS hoch |
| `import_partitions.sh` | Erstellt im `raw`-Layer neue Partitionen für die hochgeladenen Daten |
| `import_raw.sh` | Lädt die heruntergeladenen `csv`-Datei in das HDFS hoch |
| `kettle.properties` | Parameter für Kettle |

## Parameter
| Datei | Beschreibung |
|---|---|
| **Connections** | |
| `GEO_IP_CONNECTION_HDFS_SERVER` | HDFS Server IP-Adresse |
| `GEO_IP_CONNECTION_HDFS_PORT` | HDFS Server Port |
| `GEO_IP_CONNECTION_HIVE_SERVER` | HIVE Server IP-Adresse |
| `GEO_IP_CONNECTION_HIVE_PORT` | HIVE Server Port |
| `GEO_IP_CONNECTION_HIVE_DATABASE` | HIVE Datenbank |
| `GEO_IP_CONNECTION_HIVE_USERNAME` | HIVE Username |
| `GEO_IP_CONNECTION_HIVE_PASSWORD` | HIVE Passwort |
| `GEO_IP_CONNECTION_MYSQL_SERVER` | MySQL Server IP-Adresse |
| `GEO_IP_CONNECTION_MYSQL_PORT` | MySQL Server Port |
| `GEO_IP_CONNECTION_MYSQL_DATABASE` | MySQL Datenbank |
| `GEO_IP_CONNECTION_MYSQL_USERNAME` | MySQL Username |
| `GEO_IP_CONNECTION_MYSQL_PASSWORD` | MySQL Passwort |
| **Jobs und Transformationen** | |
| `GEO_IP_JOB_DOWNLOAD` | Downloaded und extrahiert die `zip`-Datei |
| `GEO_IP_JOB_IMPORT_RAW` | Importiert die heruntergeladenen Daten in den `raw`-Layer |
| `GEO_IP_JOB_IMPORT_FINAL` | Bereinigt die Daten im `raw`-Layer und speichert sie in den `final`-Layer. Mögliche Werte können in Kapitel [HIVE-Workaround](#HIVE-Workaround) nachgelesen werden. |
| `GEO_IP_JOB_IMPORT_MYSQL` | Exportiert die Daten aus den `final`-Layer in die MySQL Datenbank |
| `GEO_IP_TRANSFORM_TABLE_EMPTY_RAW` | Überprüft, ob eine Tabelle im `raw`-Layer leer ist. Berücksichtigt die Uploaddatums-Partitionen. |
| `GEO_IP_TRANSFORM_TABLE_EMPTY_FINAL` | Überprüft, ob eine Tabelle leer ist |
| `GEO_IP_TRANSFORM_IMPORT_FINAL` | Bereinigt die Daten im `raw`-Layer. Mögliche Werte können in Kapitel [HIVE-Workaround](#HIVE-Workaround) nachgelesen werden. |
| `GEO_IP_TRANSFORM_IMPORT_MYSQL` | Exportiert die Daten aus den `final`-Layer in die MySQL Datenbank |
| **Datenbank Schema** | |
| `GEO_IP_DDL_HIVE_RAW_BLOCKS` | Erstellt die IP-Adressen-Tabelle im `raw`-Layer |
| `GEO_IP_DDL_HIVE_RAW_LOCATIONS` | Erstellt die Standort-Tabelle im `raw`-Layer |
| `GEO_IP_DDL_HIVE_FINAL_DATA_IPV4` | Erstellt die IPv4-Tabelle im `final`-Layer. Mögliche Werte können in Kapitel [HIVE-Workaround](#HIVE-Workaround) nachgelesen werden. |
| `GEO_IP_DDL_HIVE_FINAL_DATA_IPV6` | Erstellt die IPv6-Tabelle im `final`-Layer. Mögliche Werte können in Kapitel [HIVE-Workaround](#HIVE-Workaround) nachgelesen werden. |
| `GEO_IP_DDL_HIVE_FINAL_LOCATIONS` | Erstellt die Standort-Tabelle im `final`-Layer |
| `GEO_IP_DDL_MYSQL_DATA_IPV4` | Erstellt die IPv4-Tabelle in der MySQL-Datenbank |
| `GEO_IP_DDL_MYSQL_DATA_IPV6` | Erstellt die IPv6-Tabelle in der MySQL-Datenbank |
| `GEO_IP_DDL_MYSQL_LOCATIONS` | Erstellt die Standort-Tabelle in der MySQL-Datenbank |
| **Datenbank Tabellennamen** | |
| `GEO_IP_TABLE_HIVE_RAW_LOCATIONS` | Tabellenname für für die IP-Adressen-Tabelle im `raw`-Layer |
| `GEO_IP_TABLE_HIVE_RAW_BLOCKS` | Tabellenname für die Standort-Tabelle im `raw`-Layer |
| `GEO_IP_TABLE_HIVE_FINAL_DATA_IPV4` | Tabellenname für die IPv4-Tabelle im `final`-Layer |
| `GEO_IP_TABLE_HIVE_FINAL_DATA_IPV6` | Tabellenname für die IPv6-Tabelle im `final`-Layer |
| `GEO_IP_TABLE_HIVE_FINAL_LOCATIONS` | Tabellenname für die Standort-Tabelle im `final`-Layer |
| `GEO_IP_TABLE_MYSQL_DATA_IPV4` | Tabellenname für die IPv4-Tabelle in der MySQL-Datenbank |
| `GEO_IP_TABLE_MYSQL_DATA_IPV6` | Tabellenname für die IPv6-Tabelle in der MySQL-Datenbank |
| `GEO_IP_TABLE_MYSQL_LOCATIONS` | Tabellenname für die Standort-Tabelle in der MySQL-Datenbank |
| **Scripte** | |
| `GEO_IP_SCRIPT_CHECK_FILES` | Überprüft, ob alle angegebenen Sprach- und IP-Dateien heruntergeladen wurden |
| `GEO_IP_SCRIPT_IMPORT_LOCATIONS_FINAL` | Bereinigt die Standort-Daten im `raw`-Layer und speichert sie in den `final`-Layer |
| `GEO_IP_SCRIPT_DOWNLOAD` | Lädt die `zip`-Datei herunter |
| `GEO_IP_SCRIPT_HDFS_PARTITIONS` | Erstellt im `raw`-Layer neue Partitionen für die hochgeladenen Daten |
| `GEO_IP_SCRIPT_HDFS_UPLOAD_RAW` | Lädt die heruntergeladenen `csv`-Datei in das HDFS hoch |
| `GEO_IP_SCRIPT_HDFS_UPLOAD_FINAL` | Lädt die generierten `csv`-Datei der IP-Adressen in das HDFS hoch. Wird nur benötigt, wenn in `GEO_IP_TRANSFORM_IMPORT_FINAL` die Datei `geo_ip_import_final_file.kjb` gesetzt ist. |
| **Sonstige Variablen** | |
| `GEO_IP_IMPORT_BLOCKS` | Semikolon-separierte Liste an IP-Versionen, welche in der Zip-Datei gelifert werden. Um alle IP-Versionen zu laden: `IPv4;IPv6` |
| `GEO_IP_IMPORT_BLOCKS_FILE_PREFIX` | Beginn des Dateinamens für die IP-Adressen. Der Bindestrich am Ende sowie die Dateiendung wird statisch gesetzt. |
| `GEO_IP_IMPORT_COMMIT_SIZE` | Gibt an, wie viele Datensätze innerhalb einer Transaction in die Datenbank geschrieben werden sollen. Um die Option zu deaktivieren: `0` |
| `GEO_IP_IMPORT_DIR` | Speichert und extrahiert die heruntergeladenen Dateien in das Verzeichnis. **Achtung: Der Ordner wird am Ende des Hauptjobs geleert!** |
| `GEO_IP_IMPORT_FILE` | Speichert die heruntergeladene `zip`-Datei unter diesem Namen |
| `GEO_IP_IMPORT_HDFS_DIR` | Lädt alle Heruntergeladenen Dateien in das HDFS-Verzeichnis |
| `GEO_IP_IMPORT_LOCATIONS` | Semikolon-separierte Liste an ISO-Language Codes, welche in der Zip-Datei gelifert werden. Um alle Sprachen zu laden: `de;en;es;fr;ja;pt-BR;ru;zh-CN` |
| `GEO_IP_IMPORT_LOCATIONS_FILE_PREFIX` | Beginn des Dateinamens für die IP-Adressen. Der Bindestrich am Ende sowie die Dateiendung wird statisch gesetzt. |
| `GEO_IP_IMPORT_URL` | Download-Link für die `zip`-Datei |
| `GEO_IP_EXPORT_BLOCKS_FILE_PREFIX` | Speichert die generierte `csv`-Datei unter diesem Namen. Für die Verwendung muss das Kapitel [HIVE-Workaround](#HIVE-Workaround) beachtet werden. |

## Datenbankdesign

### `raw`-Layer
* Heruntergeladenen Dateien werden im `raw`-Layer hochgeladen
    * -> Tabellen müssen alle Spalten der CSV besitzen
* Zur Versionierung wird das Datum gespeichert und nach diesem Partitioniert
* Für die verschiedenen Sprachen und IP-Versionen wird jeweils eine Tabelle mit verschiedenen Partitionen verwendet

### `final`-Layer
* Standorte:
    * "Unnötige" Spalten entfernen. Verblieibend:
        * Standorte Kontinent-Staat-Stadt
        * Zeitzone
* IP-Adressen:
    * `INTEGER`-Repräsentationen der IP-Adressen werden gespeichert
    * IPv4 hat 32 bits -> `UNSIGNED INTEGER`
    * IPv6 hat 128 bits -> zu große Zahl -> 2 Spalten mit `UNSIGNED BIGINT` (64 bit)
        * -> Für IPv4 und IPv6 zwei separate Tabellen benötigt

### MySQL
* Standorte:
    * Partition über die Sprachen-Spalte und Standort-ID, damit die Suche nach der richtigen Sprache beschleunigt wird
* IP-Adressen:
    * Dieselbe Struktur wie im `final`-Layer
    * Die ersten und letzten IP-Adressen werden in einem Binary Tree gehasht, um die `BETWEEN`-Suche zu beschleunigen
    * IPv4 als `UNSIGNED INTEGER`, IPv6 als `UNSIGNED BIGINT`
* Da die Standort-Tabelle partitioniert ist, kann keine Fremdschlüsselbeziehung erstellt werden

## Jobs und Transformations

### Download
* `zip`-Datei herunterladen und in ein Verzeichnis extrahieren
* Da im Verzeichnis ein Ordner mit einem "Random"-Namen existiert, in welchem sich die Dateien befinden, werden die Dateien aus den Unterordnern eine Ebene höher verschoben

### Import `raw`-Layer
* Überprüfen, ob alle angegebenen Dateien in den jeweiligen Sprachen und IP-Versionen vorhanden sind
* Falls Tabellen nicht existieren: erstellen
* Extrahierte Dateien ins HDFS hochladen. Dabei werden die definierten Partitionen in der Ordnerstruktur beachtet

### Import `final`-Layer
* Überprüfen, ob es Daten im `raw`-Layer gibt
* Falls Tabellen nicht existieren: erstellen
* Standorte:
    * Nicht mehr benötigte Spalten entfernen
* IP-Adressen:
    * IP-Adresse und Subnetz-Maske trennen
    * Zahlenwert aus den IP-Adressen ermitteln (erste IP-Adresse)
    * Netzwerkmaske addieren (letzte IP-Adresse)

### Import MySQL
* Überprüfen, ob es Daten im `final`-Layer gibt
* Falls Tabellen nicht existieren: erstellen
* Daten aus HIVE laden und in MySQL speichern

## HIVE-Workaround
Wenn man über eine Transformation direkt in eine HIVE-Tabelle schreiben möchte, wird für jeden Datensatz eine neue Verbindung aufgebaut.
Da ich in keine Einstellung finden konnte, um mehrere Datensätze in einer Verbindung verschicken zu können, habe ich mich für einen `csv`-Dateiexport entschieden, der analog zum `raw`-Layer funktioniert.
In den Projektdateien befinden sich allerdings beide Versionen, welche sich über die Kettle-Parameter umschalten lassen:

| Parameter | Direkter Datenbankzugriff | File Export |
|---|---|---|
| `GEO_IP_JOB_IMPORT_FINAL` | `/home/hadoop/geo_ip/geo_ip_import_final_db.kjb` | `/home/hadoop/geo_ip/geo_ip_import_final_file.kjb` |
| `GEO_IP_TRANSFORM_IMPORT_FINAL` | `/home/hadoop/geo_ip/geo_ip_import_final_db.ktr` | `/home/hadoop/geo_ip/geo_ip_import_final_file.ktr` |
| `GEO_IP_DDL_HIVE_FINAL_DATA_IPV4` | `/home/hadoop/geo_ip/geo_ip_hive_final_data_ipv4_db.sql` | `/home/hadoop/geo_ip/geo_ip_hive_final_data_ipv4_file.sql` |
| `GEO_IP_DDL_HIVE_FINAL_DATA_IPV6` | `/home/hadoop/geo_ip/geo_ip_hive_final_data_ipv4_db.sql` | `/home/hadoop/geo_ip/geo_ip_hive_final_data_ipv4_file.sql` |
