<?php
$servername = "127.0.0.1";
$username = "hadoop";
$password = "hadoop";
$dbname = "geo_ip";

$ip_address = $_GET["ip_address"];
$location = $_GET["location"];

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
$response = array();

// Check connection
if ($conn->connect_error) {
    $response["success"] = false;
    $response["message"] = $conn->connect_error;
    die(json_encode($response));
}

if (filter_var($ip_address, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
    $ip_address = ip2long($ip_address);
    $stmt = $conn->prepare("
        SELECT
            ip.network,
            l.continent_name,
            l.country_name,
            l.city_name,
            l.metro_code,
            l.time_zone,
            ip.postal_code,
            ip.latitude,
            ip.longitude,
            ip.accuracy_radius
        FROM geo_ip_data_ipv4 AS ip
        INNER JOIN geo_ip_locations AS l ON ip.geoname_id = l.geoname_id
        WHERE l.partition_location = ?
            AND ? BETWEEN ip.subnet_begin AND ip.subnet_end");
    $stmt->bind_param("si", $location, $ip_address);
} else if (filter_var($ip_address, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
    $ip_address = explode(":", $ip_address);
    foreach ($ip_address as &$value) {
        $value = hexdec($value);
    }
    $ip1 = $ip_address[0]*65536*65536*65536+$ip_address[1]*65536*65536+$ip_address[2]*65536+$ip_address[3];
    $ip2 = $ip_address[4]*65536*65536*65536+$ip_address[5]*65536*65536+$ip_address[6]*65536+$ip_address[7];
    $stmt = $conn->prepare("
        SELECT
            ip.network,
            l.continent_name,
            l.country_name,
            l.city_name,
            l.metro_code,
            l.time_zone,
            ip.postal_code,
            ip.latitude,
            ip.longitude,
            ip.accuracy_radius
        FROM geo_ip_data_ipv6 AS ip
        INNER JOIN geo_ip_locations AS l ON ip.geoname_id = l.geoname_id
        WHERE l.partition_location = ?
            AND ".$ip1." BETWEEN ip.subnet_begin_1 AND ip.subnet_end_1
            AND ".$ip2." BETWEEN ip.subnet_begin_2 AND ip.subnet_end_2");
    $stmt->bind_param("s", $location);
} else {
    $response["success"] = false;
    $response["message"] = "Please enter a valid IPv4 or IPv6 address";
    die(json_encode($response));
}

$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows > 0) {
    $data = $result->fetch_object();
    $response["success"] = true;
    $response["network"] = $data->network;
    $response["continent_name"] = $data->continent_name;
    $response["country_name"] = $data->country_name;
    $response["city_name"] = $data->city_name;
    $response["metro_code"] = $data->metro_code;
    $response["time_zone"] = $data->time_zone;
    $response["postal_code"] = $data->postal_code;
    $response["latitude"] = $data->latitude;
    $response["longitude"] = $data->longitude;
    $response["accuracy_radius"] = $data->accuracy_radius;
} else {
    $response["success"] = false;
    $response["message"] = "No results...";
}

echo json_encode($response);

$conn->close();
?>
