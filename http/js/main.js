let locations;

function getInfos(ip, panel) {
    $('#search').prop('disabled', true);
    $('#searching-panel').removeClass('hide');
    $.ajax({
        method: 'GET',
        url: './get_infos.php',
        data: {
            location: $('#locations').val(),
            ip_address: ip
        },
        success: function(response) {
            let result = JSON.parse(response);

            $('#search').prop('disabled', false);
            $('#searching-panel').addClass('hide');

            if (result.success) {
                panel.find('.address').text(ip);
                panel.find('.subnet').text(result.network);
                panel.find('.continent').text(result.continent_name);
                panel.find('.country').text(result.country_name);
                panel.find('.city').text(result.city_name);
                panel.find('.postal').text(result.postal_code);
                panel.find('.metro').text(result.metro_code);
                panel.find('.timezone').text(result.time_zone);
                panel.find('.latitude').text(result.latitude);
                panel.find('.longitude').text(result.longitude);
                panel.find('.radius').text(result.accuracy_radius);
                panel.removeClass('hide');
                $('#error-panel').addClass('hide');
            } else {
                $('#error-panel').removeClass('hide');
                $('#error-message').text(result.message);
            }
        }
    });
}

$(function() {
    // Init location dropdown
    $.ajax({
        method: 'GET',
        url: './get_locations.php',
        success: function(response) {
            let result = JSON.parse(response);
            let locations = [];

            result.forEach(function(location) {
                locations.push({
                    value: location,
                    label: location,
                    selected: false,
                    disabled: false,
                });
            });

            new Choices('#locations', {
                choices: locations,
                classNames: {
                    containerOuter: 'choices locations'
                }
            });

            // Get current IP
            $.ajax({
                method: 'GET',
                url: 'https://httpbin.org/ip',
                success: function(response) {
                    getInfos(response.origin, $('#your-ip-panel'));
                }
            });
        }
    });

    // Handle enter click
    $(document).ready(function() {
        $(window).keydown(function(event) {
          if (event.keyCode == 13) {
            event.preventDefault();
            getInfos($('#search').val(), $('#search-ip-panel'));
            return false;
          }
        });
      });
});
