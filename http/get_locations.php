<?php
$servername = "127.0.0.1";
$username = "hadoop";
$password = "hadoop";
$dbname = "geo_ip";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
$response = array();

// Check connection
if ($conn->connect_error) {
    $response["success"] = false;
    $response["message"] = $conn->connect_error;
    die(json_encode($response));
}

$stmt = $conn->prepare("
    SELECT DISTINCT
        partition_location AS location
    FROM geo_ip_locations");

$stmt->execute();
$result = $stmt->get_result();

while ($row = $result->fetch_object()) {
    array_push($response, $row->location);
}

echo json_encode($response);
$conn->close();
?>
